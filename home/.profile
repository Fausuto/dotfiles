# [[ -f ~/.bashrc ]] && . ~/.bashrc

if [ "$SHELL" = '/bin/bash' ];then
    . ~/.bashrc
else [ "$SHELL" = '/bin/mksh' ];
    source ~/.mkshrc
fi

PATH=$PATH:~/.local/bin:~/.gem/ruby/2.7.0/bin

# Main Progams
export EDITOR=nvim
export VISUAL=nvim
export FILE=fff
export READER="zathura"
export LESSHISTFILE=-
export TERMINAL=st-256color
export BROWSER=firefox

# Cleaned Up $HOME
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CACHE_HOME=$HOME/.cache
export GOPATH=$XDG_CONFIG_HOME/go
export GEM_PATH=$XDG_CONFIG_HOME/gem
export GEM_HOME=$XDG_CONFIG_HOME/gem
export CARGO_HOME=$XDG_DATA_HOME/cargo
export GTK2_RC_FILES=$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0
export GNUPGHOME=$XDG_DATA_HOME/gnupg
export XAUTHORITY=$XDG_DATA_HOME/sx/xauthority
export XENVIRONMENT=$XDG_CONFIG_HOME/x/xresources

# Specific Programs
export _JAVA_AWT_WM_NONREPARENTING=1
export QT_QPA_PLATFORMTHEME=qt5ct
export FFF_OPENER="zathura"
export FFF_OPENER="mpv"
export PF_INFO="ascii\
                title\
                os\
                host\
                kernel\
                uptime\
                pkgs\
                memory\
                editor\
                shell\
                wm\
                palette"
