export PATH="${PATH}:${HOME}/.local/bin"

export EDITOR="nvim"
export TERMINAL="st"
export FILE="fff"
export READER="zathura"
export BROWSER="firefox"

export ZDOTDIR="$HOME/.config/zsh"

# FFF Settings
# w3m-img offsets.
export FFF_W3M_XOFFSET=40
export FFF_W3M_YOFFSET=25
export FFF_CD_ON_EXIT=1

# Autostart X at login
if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
    exec startx
fi
