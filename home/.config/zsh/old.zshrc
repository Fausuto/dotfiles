# Lines configured by zsh-newuser-install
HISTFILE=~/.config/zsh
HISTSIZE=5000
SAVEHIST=500
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/luxray/.zshrc'

autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
compinit
_comp_options+=(globdots)   # include hidden files
# End of lines added by compinstall

# Prompt theme customization
#jautoload -Uz promptinit; promptinit
#zstyle :prompt:pure:path color white
#prompt pure

PROMPT='%B> %1~ '

#
# WAL STUFF APPLY THEME TO NEW TERMINALS
# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
(cat ~/.cache/wal/sequences &)
#

#
# Aliases
alias df='df | grep /dev/sd'
alias ls='ls --color=auto --group-directories-first'
alias ll='ls --group-directories-first -al'
#alias scrot='scrot '%Y-%m-%d_$wx$h.png' -e 'mv $f ~/Downloads/''
#

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
# source /usr/bin/promptless.sh
