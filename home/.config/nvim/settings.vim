"
" Tabs and spaces configuration
"
set tabstop=4               " Number of visual spaces per TAB
set shiftwidth=4
set softtabstop=4           " Number of spaces in tab when editing
set expandtab               " Insert spaces when tab is pressed
set shiftround              " Tab/Shifting moves to closest tabstop
set smartindent             " Intelligently dedent/indent new lines based on rules


"
" UI configuration that changes NEOVIM visually
"
set noshowmode              " Bottom left, hides -- INSERT -- and -- VISUAL --
set number                  " Show the number lines on the left side
set relativenumber          " Turn relative numbers on
" set cursorline              " Highlight current lines
set lazyredraw              " Redraw only when we need to
set showtabline=0
set laststatus=0
set listchars=tab:▸\ ,trail:·,eol:¬,nbsp:_
set breakindent
set modeline



set ignorecase              " Case insensitive search
set smartcase               " If there are uppercase letters, become case-sensitive
set showmatch                " Highlight matching [{()}]
set gdefault                " Use the 'g' flag by default



set shiftwidth=4            " Indentation amount for < or > commands
set nojoinspaces            " Prevents insterting two spaces after punctuations on a join (J)
set splitbelow              " Horizontal split below current
set splitright              " Vertical split to right of current
"set nowrap                  " Automatically wrap text that extends beyond the screen length
set scrolloff=5             " Display 5 lines above & below the cursor when scrolling with a mouse
set mouse=a                 " Enable the use of your mouse
set clipboard=unnamedplus   " Be able to paste into neovim from anywhere
" set colorcolumn=80

"
" These are all the settings that I have found to be default in NEOVIM. Which
" I had set using VIM before
"
"set smarttab                " NVIM DEFAULT, let's tab key insert 'tab stops', and bksp deletes tabs
"set autoindent              " NVIM DEFAULT, Match indents on new lines
"set showcmd                 " NVIM DEFAULT, Show command in bottom bar
"filetype indent on          " NVIM DEFAULT, load filetype-specific indent files
"set wildmenu                " NVIM DEFAULT, Visual autocomplete for command menu
"set incsearch               " NVIM DEFAULT, Search as characters are entered
"set hlsearch                " NVIM DEFAULT, Highlight matches
"set formatoptions+=o        " NVIM DEFAULT, Continue comment marker in new lines
"set encoding=utf-8          " NVIM DEFAULT, Encoding
