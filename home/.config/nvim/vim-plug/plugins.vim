" Automatic installation of vim-plug, a minimalistic plug in manager. Very
" useful when symlink init.vim on a new system. Found this tip in
" junegun/vim-plug wiki, and autogroup from dylanaraps
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

  augroup PLUG
      au!
      autocmd VimEnter * PlugInstall --sync | source ~/.config/nvim/init.vim
  augroup END
endif
"
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.config/nvim/plugged')
"
" Make sure you use single quotes
"
" Declare the list of plugins
"
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
" Plug 'rstacruz/vim-closer'
Plug 'maxboisvert/vim-simple-complete'
Plug 'w0rp/ale'
Plug 'habamax/vim-asciidoctor'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
" Plug 'dylanaraps/fff.vim'
Plug 'mcchrish/nnn.vim'
Plug 'deviantfero/wpgtk.vim'
" Plug 'itchyny/lightline.vim'
"Plug 'rbong/vim-crystalline'
"
" List ends here. Plugins become visible after this. Initialize plugin system
call plug#end()
