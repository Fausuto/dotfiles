source $HOME/.config/nvim/settings.vim
source $HOME/.config/nvim/keymaps.vim

source $HOME/.config/nvim/vim-plug/plugins.vim
source $HOME/.config/nvim/vim-plug_configs/vim-simple-complete.vim
source $HOME/.config/nvim/vim-plug_configs/vim-asciidoctor.vim
source $HOME/.config/nvim/vim-plug_configs/goyo.vim
source $HOME/.config/nvim/vim-plug_configs/nnn.vim
source $HOME/.config/nvim/vim-plug_configs/wpgtk.vim
" source $HOME/.config/nvim/vim-plug_configs/lightline.vim
" source $HOME/.config/nvim/vim-plug_configs/vim-crystalline.vim
source $HOME/.config/nvim/vim-plug_configs/vimtex.vim

" highlight CursorLine term=bold cterm=bold ctermbg=8 ctermfg=none
highlight CursorLineNr term=bold cterm=bold ctermbg=none ctermfg=7
" highlight ColorColumn term=bold cterm=bold ctermbg=8 ctermfg=none
highlight Comment ctermbg=none ctermfg=8

let g:netrw_liststyle = 3
let g:netrw_banner = 0
let g:netrw_winsize = 20
let g:netrw_browse_split = 4
let g:netrw_altv = 1
