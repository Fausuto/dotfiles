" Settings/Configurations for Goyo when entering and leaving
" Also disables showmode showcmd when entering and reenables
function! s:goyo_enter()
    set noshowmode
	set noshowcmd
	set scrolloff=999
	Limelight
	" ...
endfunction
"
function! s:goyo_leave()
    set showmode
	set showcmd
	set scrolloff=5
	Limelight!
	" ...
endfunction
"
autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
"
" Color name (:help cterm-colors) or ANSI code
let g:limelight_conceal_ctermfg = '235'
let g:limelight_default_coefficient = '0.1'
