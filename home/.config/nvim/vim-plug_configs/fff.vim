" Vertical split (NERDtree style).
let g:fff#split = "30vnew"

" Open fff on press of 'f'
nnoremap f :F<CR>
