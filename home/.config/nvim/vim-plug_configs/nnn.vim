" Opens the nnn window in a split
let g:nnn#layout = 'vnew' " new, vnew, tabnew

" Or pass a dictionary with a window size
let g:nnn#layout = { 'left': '~20%' } " left, right, up, down
