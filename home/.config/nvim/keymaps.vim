" Leader is a key that allows you to have your own 'namespaces' of keybindings.
" You'll see it a lot farther below as <leader>
let mapleader = " "

" Turn off search highlight
nnoremap <leader><space> :nohlsearch<CR> 

" So we don't have to press shift when we want to get into command line
nnoremap ; :
vnoremap ; :

" Easier escaping from INSERT and COMMNAND MODE
inoremap ii <ESC>
cnoremap ii <ESC>
vnoremap ii <ESC>

" Be able to move lines up or down when highlighting with visual line
xnoremap K :move '<-2<CR>gv-gv
xnoremap J :move '>+1<CR>gv-gv

" Disable so I don't accidentally go into :exec mode
nnoremap Q <nop>

" Helps with moving between splits using leader
nnoremap <leader>h :wincmd h <CR>
nnoremap <leader>j :wincmd j <CR>
nnoremap <leader>k :wincmd k <CR>
nnoremap <leader>l :wincmd l <CR>

" Opens netrw in a left split
nnoremap <leader>nt :Lex <bar> :vertical resize 30<CR>

" Pressing Enter will launch Goyo
map <ENTER> :Goyo<CR>
