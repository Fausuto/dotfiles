# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='\[\033[1;32m\]❯ \W\[\033[0m\] '

# Vi functionality; pressing Esc
set -o vi
# Permanently disable bash history
# set +o history
unset HISTFILE

# When Tab, using cd, only show directories
complete -d cd


bind 'TAB:menu-complete'
bind 'set show-all-if-ambiguous on'
bind 'set completion-ignore-case on'
bind 'set menu-complete-display-prefix on'
bind 'set colored-stats on'
#bind 'set completion-display-width 1'
#bind Space:magic-space


shopt -s \
    expand_aliases


#
# Aliases
alias df='df -Hh | grep /dev/sd'
alias ls='ls -phN --color=always'
alias la='ls -AphN --color=always'
alias ll='ls -AphNl --color=always'
alias td='transmission-daemon'
alias ktd='killall transmission-daemon'
alias n='nvim'
alias nb='newsboat'
alias pb='podboat'
alias p='printf "%s\n" ${PWD/#$HOME/"~"}'
#alias firefox='firefox -p'

# Add this to your .bashrc, .zshrc or equivalent.
# Run 'fff' with 'f' or whatever you decide to name the function.
f() {
    fff "$@"
    cd "$(cat "${XDG_CACHE_HOME:=${HOME}/.cache}/fff/.fff_d")"
}

#
# Pywal Stuff
# Import colorscheme from 'wal' asynchronously
# &     # Run the process in the background
# ( )   # Hids shell job control messages
#(cat ~/.cache/wal/sequences &)
(cat $HOME/.config/wpg/sequences &)
#
# To add support for TTYs this line can be optionally added
source ~/.cache/wal/colors-tty.sh
#
. "$HOME/.cache/wal/colors.sh"
